import pytest
from delune.searcher.segment import fieldreader
from delune import standard_analyzer

@pytest.fixture
def sentence ():
    return """
Arthur Conan Doyle (1859–1930), Sherlock Holmes' creator, in 1914
Edgar Allan Poe's C. Auguste Dupin is generally acknowledged as the first detective in fiction and served as the prototype for many that were created later, including Holmes.[6] Conan Doyle once wrote, "Each [of Poe's detective stories] is a root from which a whole literature has developed... Where was the detective story until Poe breathed the breath of life into it?"[7] Similarly, the stories of Émile Gaboriau's Monsieur Lecoq were extremely popular at the time Conan Doyle began writing Holmes, and Holmes' speech and behaviour sometimes follow that of Lecoq.[8] Both Dupin and Lecoq are referenced at the beginning of A Study in Scarlet.
    """
@pytest.fixture
def animals ():
    return """
Lion Lions cat Cat cAts CATS dog dogs DOgs Snake <snake> </snake>
    """
    
@pytest.fixture
def freader ():    
    class Segment:
        def getTermInfo (self, *args, **kargs):
            return (100,)
        
        def getTermInfos (self, *args, **kargs):
            return [(100,)]
        
        def getTermInfos2 (self, *args, **kargs):
            return [(100,)]
         
        def getSortMapPointer (self, *args, **kargs):
            return 200
    
    ana = standard_analyzer (8)    
    ana.setopt (ngram = True, stem_level = 2, make_lower_case = True)
    return fieldreader.FieldReader (None, Segment (), ana, False)
    
