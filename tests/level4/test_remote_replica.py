import os
import pytest
import os
import csv
import shutil
import delune
from delune.fields import *
from rs4 import pathtool, logger
from delune.cli import indexer, replicator
import shutil
import time

def index_and_wait (rdir, engine):
    indexer.index (rdir)
    for i in range (3):
        r = engine.get ("/delune/status")
        time.sleep (1)

def test_remote_index (naics, rdir, launch, _index):
    delune.configure (1, logger.screen_logger ()) # for indexer
    confdir = os.path.join (rdir, "delune", "config")
    coldir = os.path.join (rdir, "delune", "collections")

    with launch (os.path.join (os.path.dirname (os.path.dirname (__file__)), "app.py")) as engine:
        dl = delune.mount ("http://localhost:30371/delune")

        col = dl.create ("testcol", ["testcol"])
        col.setopt ("analyzer", make_lower_case = True, stem_level = 1)
        _index (col, naics, 500, count = 100)
        col.commit ()
        # time.sleep (1000)
        index_and_wait (rdir, engine)

        rdir2 = rdir + "2"
        replicator.replicate ("http://localhost:30371/delune", rdir2, True)
        assert os.path.isfile (os.path.join (rdir2, "delune", "config", "testcol"))
        assert os.path.isdir (os.path.join (rdir2, "delune", "collections", "testcol"))
        assert os.path.isfile (os.path.join (rdir2, "delune", "collections", "testcol", "segments"))
        assert os.path.isfile (os.path.join (rdir2, "delune", "collections", "testcol", "0.fda"))

        col.close ()
        col.drop (True)
        assert not os.path.isdir (os.path.join (confdir, "testcol"))
        shutil.rmtree (rdir2)

