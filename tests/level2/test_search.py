import os
import pytest
import os
import csv
import shutil
import delune
from delune.fields import *
from rs4 import pathtool
          
def test_index (naics, rdir, _index):
    confdir = os.path.join (rdir, "delune", "config")
    coldir = os.path.join (rdir, "delune", "collections")
    
    dl = delune.mount (rdir)
    assert os.path.isdir (confdir)
    assert os.path.isdir (coldir)
    
    col = dl.create ("testcol", ["testcol"])
    col.setopt ("analyzer", make_lower_case = True, stem_level = 1)
    assert os.path.isfile (os.path.join (confdir, "testcol"))
    
    with col.documents as ds:
        ds.truncate ("testcol")    
        col.commit ()
        dl.index ()
        
    _index (col, naics, 500)
    col.commit ()    
    dl.index ()
    
    col = dl.load ("testcol")
    with col.documents as ds:
        assert ds.search ("service") ['total'] == 225
        assert ds.search ("services") ['total'] == 225
        
        assert ds.search ("packaging services") ['total'] == 2
        assert ds.search ("service packaging") ['total'] == 2
        assert ds.search ("code:5416 services") ['total'] == 1
        assert ds.search ("code:5416,5417") ['total'] == 2
        assert ds.search ("code:5416 or code:5417") ['total'] == 2
        assert ds.search ("services -packaging") ['total'] == 223
                
        assert ds.search ('"grain farming"') ['total'] == 3
        assert ds.search ('grain farming') ['total'] == 4
        assert ds.search ('grain farming -"grain farming"') ['total'] == 1
        assert ds.search ('grain farming -"grain fming"') ['total'] == 4
        assert ds.search ('"grain fming"') ['total'] == 0
        
        assert ds.search ('surveying') ['total'] == 4
        assert ds.search ('building') ['total'] == 37
        assert ds.search ('surveying building') ['total'] == 0
        assert ds.search ('surveying or building') ['total'] == 41
        
        assert ds.search ('term:"grain farming"') ['total'] == 4 # TERM dose not support phrase
        assert ds.search ('(term:grain farming)') ['total'] == 4
        
        assert ds.search ('rownum:0~100') ['total'] == 100
        assert ds.search ('rownum:1000~1100') ['total'] == 100        
        assert ds.search ('farming rownum:0~100') ['total'] == 55
        print (ds.search ('farming rownum:0~100', sort = "-rownum"))
        
        r = ds.search ('rownum:0~100', sort = "+rownum")
        assert r ['total'] == 100
        assert r ["result"][0][0]["rownum"] == 0
        
        r = ds.search ('rownum:0~100', sort = "rownum")
        assert r ['total'] == 100
        assert r ["result"][0][0]["rownum"] == 99
        
        r = ds.search ('rownum:0~100', sort = "-rownum")
        assert r ['total'] == 100
        assert r ["result"][0][0]["rownum"] == 99
        
        r = ds.search ('rownum:0~100', sort = "-rownumbit")
        assert r ['total'] == 100
        assert r ["result"][0][0]["rownum"] == 99
        
        r = ds.search ('rownum:0~100', sort = "+rownumbit")
        assert r ['total'] == 100
        assert r ["result"][0][0]["rownum"] == 0
        
        r = ds.search ('(goat or sheep or hog or pig or fish)', sort = "tfidf")
        assert r ['total'] == 13
        origin = [ v[-1] for v in r ["result"]]
        assert origin == sorted (origin [:], reverse = 1) 
        
        r = ds.search ('(goat or sheep or hog or pig or fish)', sort = "-tfidf")
        assert r ['total'] == 13
        origin = [ v[-1] for v in r ["result"]]
        assert origin == sorted (origin [:], reverse = 1)
        
        r = ds.search ('(goat or sheep or hog or pig or fish)', sort = "+tfidf")
        assert r ['total'] == 13
        origin = [ v[-1] for v in r ["result"]]
        assert origin == sorted (origin [:], reverse = 0)
                 
        r = ds.search ('(goat or sheep or hog or pig or fish) rownum:1111/any')
        assert r ['total'] == 10
        
        r = ds.search ('(goat or sheep or hog or pig or fish) rownum:1111/all')
        assert r ['total'] == 1
        
        r = ds.search ('(goat or sheep or hog or pig or fish) rownum:0001/none')
        assert r ['total'] == 7
        
        r = ds.search ('coord:30/30~1000000', sort = "-coord", limit = 200)
        assert r ['total'] == 169
        origin = [ v[-1] for v in r ["result"]]
        assert origin == sorted (origin [:], reverse = 0)
        
        r = ds.search ('icode:1100~1400')
        assert r ['total'] == 301
        
        r = ds.search ('icode:110~140')
        assert r ['total'] == 31
        
        r = ds.search ('she*')        
        assert r ['total'] == 15
        
        r = ds.search ('she* farming')        
        assert r ['total'] == 4
        
    #-------------------------------------------------------
    col.close ()
    col.drop (True)
    assert not os.path.isdir (os.path.join (confdir, "testcol"))
    
    